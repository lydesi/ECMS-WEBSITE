export default {
  methods: {
    getSrc(item) {
      if(item.logo) {
        return 'data:' + item.logoContentType + ';base64,' + item.logo
      }

      return 'https://bootstrapshuffle.com/placeholder/pictures/bg_16-4.svg'
    },
    openFile(contentType, data) {
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        // To support IE and Edge
        const byteCharacters = atob(data);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], {
          type: contentType,
        });
        window.navigator.msSaveOrOpenBlob(blob);
      } else {
        // Other browsers
        const fileURL = `data:${contentType};base64,${data}`;
        const win = window.open();
        win.document.write(
          '<iframe src="' +
          fileURL +
          '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>'
        );
      }
    }
  }
}
