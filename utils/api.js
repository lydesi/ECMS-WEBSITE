export default {
  auth: {
    register: () => {
      return 'register'
    }
  },
  groups: {
    fetch: () => {
      return 'groups'
    },
    fetchById: (id) => {
      return `groups/${id}`
    }
  },
  profile: {
    fetch: () => {
      return 'profile/current'
    }
  },
  subjects: {
    fetchById: (id) => {
      return `subjects/${id}`
    }
  },
  posts: {
    fetch: () => {
      return 'posts'
    }
  },
  testings: {
    fetch: () => {
      return 'testings'
    }
  }
}
