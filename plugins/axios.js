export default function ({ $axios, $config, $i18n, error }, inject) {
  const axios = $axios.create({})
  axios.setBaseURL($config.apiURL)

  axios.interceptors.response.use(
    (response) => {
      return response
    },
    (err) => {
      const code = parseInt(
        err.response && err.response.status, 10
      )

      const codeWhiteList = [401, 400]

      if (!codeWhiteList.includes(code)) {
        error({ statusCode: code })
      }

      return Promise.reject(err)
    }
  )

  inject('axios', axios)
}
