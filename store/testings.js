import api from "../utils/api";

export const state = () => ({
  testings: []
})

export const getters = {
  GET_TESTINGS: state => state.testings
}

export const mutations = {
  SET_TESTINGS (store, data) {
    store.testings = data
  }
}

export const actions = {
  async FETCH_TESTINGS ({ commit }, subjectId) {
    const response = await this.$axios.get(api.testings.fetch(), {
      params: {
        'subjectId.equals': subjectId
      }
    })
    commit('SET_TESTINGS', response.data)
  }
}
