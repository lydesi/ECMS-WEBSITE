import api from "../utils/api";

export const state = () => ({
  groups: [],
  user_group: {}
})

export const getters = {
  GET_GROUPS: state => state.groups,
  GET_GROUP: state => state.user_group,
  GET_GROUP_SUBJECTS: state => state.user_group.subjects
}

export const mutations = {
  SET_GROUPS (store, data) {
    store.groups = data
  },
  SET_USER_GROUP (store, data) {
    store.user_group = data
  }
}

export const actions = {
  async FETCH_GROUPS ({ commit }) {
    const response = await this.$axios.get(api.groups.fetch())
    commit('SET_GROUPS', response.data)
  },

  async FETCH_USER_GROUP ({ commit }) {
    const response = await this.$axios.get(api.groups.fetchById(this.$auth.user.group.id))
    commit('SET_USER_GROUP', response.data)
  }
}
