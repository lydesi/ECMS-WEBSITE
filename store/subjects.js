import api from "../utils/api";

export const state = () => ({
  subject: {}
})

export const getters = {
  GET_SUBJECT: state => state.subject
}

export const mutations = {
  SET_SUBJECT(store, data) {
    store.subject = data
  }
}

export const actions = {
  async FETCH_SUBJECT ({ commit }, id) {
    const response = await this.$axios.get(api.subjects.fetchById(id))
    commit('SET_SUBJECT', response.data)
  }
}
