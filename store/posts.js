import api from "../utils/api";

export const state = () => ({
  posts: [],
  post: {}
})

export const getters = {
  GET_POSTS: state => state.posts,
  GET_POST: state => state.post
}

export const mutations = {
  SET_POSTS (store, data) {
    store.posts = data
  },
  SET_POST (store, data) {
    store.post = data
  }
}

export const actions = {
  async FETCH_POSTS ({ commit }, subjectId) {
    const response = await this.$axios.get(`${api.posts.fetch()}`, {
      params: {
        'subjectId.equals': subjectId
      }
    })

    commit('SET_POSTS', response.data)
  },

  async FETCH_POST ({ commit }, id) {
    const response = await this.$axios.get(`${api.posts.fetch()}/${id}`)
    commit('SET_POST', response.data)
  }
}
