import api from "../utils/api";

export const actions = {
  async SING_IN ({}, credentials) {
    return await this.$auth.loginWith('local', { data: credentials })
  },

  async REGISTER ({ commit }, data) {
    return await this.$axios.post(api.auth.register(), data)
      .then((res) => {
        return res
      })
      .catch((err) => {
        return err.response
      })
  },
}

export const getters = {
  GET_USER: state => state.user
}
